namespace FINEDAYAPI.DTO
{
    public class FoodDTO
    {
        public int FoodId { get; set; }
        public int ShopId { get; set; }
        public string FoodName { get; set; }
        public int? TypeFood { get; set; }  
    }
}