using System.Collections.Generic;
using FINEDAYAPI.Models;

namespace FINEDAYAPI.DTO
{
    public class ShopDTO
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string OwnerName { get; set; }
        public string ShopAddress { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; } 
        public  ICollection<FoodDTO> Food { get; set; } 

    }
}