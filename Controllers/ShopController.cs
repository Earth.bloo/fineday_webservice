using System.Collections.Generic;
using FINEDAYAPI.DTO;
using FINEDAYAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FINEDAYAPI.Controllers
{
    [ApiController]
    [Route("api/shop")]
    public class ShopController : ControllerBase
    {
        private readonly ShopRepository _shopRepository;
        public ShopController(ShopRepository shopRepository)
        {
            _shopRepository = shopRepository;
        }

        [HttpGet]
        public IActionResult getAll(){
            var shopitem = _shopRepository.GetAll();
            if(shopitem == null){
               return NotFound(); 
            }
            
            List<ShopDTO> response = new List<ShopDTO>();
            foreach (var shop in shopitem)
            {
                List<FoodDTO> responseFood = new List<FoodDTO>();
                foreach(var food in shop.Foods)
                {
                responseFood.Add(new FoodDTO()
                {
                    FoodId = food.FoodId,
                    FoodName = food.FoodName,
                    TypeFood = food.TypeFood
                });
                    
                }
                response.Add(new ShopDTO() 
                {
                 ShopId = shop.ShopId,
                 ShopName = shop.ShopName,
                 OwnerName = shop.OwnerName,
                 ShopAddress = shop.ShopAddress,
                 Lat = shop.Lat,
                 Long = shop.Long,
                 Food = responseFood
                });
            }
            return StatusCode(StatusCodes.Status200OK,response);
        }
    }
}