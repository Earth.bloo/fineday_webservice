﻿using System;
using System.Collections.Generic;

namespace FINEDAYAPI.Models
{
    public partial class Shops
    {
        public Shops()
        {
            Foods = new HashSet<Foods>();
        }

        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string OwnerName { get; set; }
        public string ShopAddress { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }

        public virtual ICollection<Foods> Foods { get; set; }
    }
}
