﻿using System;
using System.Collections.Generic;

namespace FINEDAYAPI.Models
{
    public partial class Foods
    {
        public int FoodId { get; set; }
        public int ShopId { get; set; }
        public string FoodName { get; set; }
        public int? TypeFood { get; set; }

        public virtual Shops Shop { get; set; }
    }
}
