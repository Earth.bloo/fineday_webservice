﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FINEDAYAPI.Models
{
    public partial class FineDayContext : DbContext
    {
        public FineDayContext()
        {
        }

        public FineDayContext(DbContextOptions<DbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Foods> Foods { get; set; }
        public virtual DbSet<Shops> Shops { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=FineDayDB;Trusted_Connection=False;User ID=sa;Password=Earth29chok");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Foods>(entity =>
            {
                entity.HasKey(e => e.FoodId)
                    .HasName("PK__Foods__856DB3CB06C13402");

                entity.Property(e => e.FoodId)
                    .HasColumnName("FoodID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FoodName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShopId).HasColumnName("ShopID");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.Foods)
                    .HasForeignKey(d => d.ShopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ShopID");
            });

            modelBuilder.Entity<Shops>(entity =>
            {
                entity.HasKey(e => e.ShopId)
                    .HasName("PK__Shops__67C556297BB8247D");

                entity.Property(e => e.ShopId)
                    .HasColumnName("ShopID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Lat)
                    .HasColumnName("LAT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Long)
                    .HasColumnName("LONG")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShopAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShopName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
