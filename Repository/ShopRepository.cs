using FINEDAYAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FINEDAYAPI.Repository
{
    public class ShopRepository : IRepository<Shops>
    {
         private readonly FineDayContext _context;
        public ShopRepository(FineDayContext context)
        {
            _context = context;
        }


         public IEnumerable<Shops> GetAll()
        {
             return _context.Shops
                .Include(j => j.Foods)
                .ToList();
        }

        public void Add(Shops entity)
        {
        }

        public void Delete(Shops entity)
        {
            throw new System.NotImplementedException();
        }

        public Shops Get(long id)
        {
            throw new System.NotImplementedException();
        }


        public void Update(Shops entityToUpdate, Shops entity)
        {
            throw new System.NotImplementedException();
        }
    }
}